use crate::open_state::OpenState;
use crate::loading_state::LoadingState;

use eframe::egui::{self, Color32, RichText};

/// Top-level GUI state
pub enum MainState {
    NothingOpen(Option<String>),
    /// A project is being opened
    Loading(LoadingState),
    /// A project is open
    Open(OpenState),
}

impl MainState {
    /// draws UI based on current state and handles state change
    pub fn ui(self, ui: &mut egui::Ui) -> MainState {
        match self {
            MainState::NothingOpen(error) => {
                ui.heading("No project open");

                if ui.button("📂 Open project").clicked() {
                    return MainState::Loading(LoadingState::open_project());
                }
                if ui.button("➕ New project").clicked() {
                    return MainState::Loading(LoadingState::create_project());
                }
                if let Some(error) = &error {
                    ui.label(RichText::new(error).color(Color32::RED));
                }
                return MainState::NothingOpen(error);
            }
            MainState::Loading(loading_state) => loading_state.ui(ui),
            MainState::Open(open_state) => open_state.ui(ui),
        }
    }
}