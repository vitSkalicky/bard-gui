use crate::loading_state::LoadingState;
use crate::main_state::MainState;
use crate::open_state::OpenState;
use crate::ProjectExt;

use anyhow::Result;
use bard::project::Project;
use eframe::egui::{self, Color32, RichText, TextEdit};
use poll_promise::Promise;
use reqwest::Url;
use std::fs::File;
use std::io::prelude::*;

/// GUI for importing songs from supported websites
pub enum ImportState {
    /// Ask user for song url
    EnterURL(Box<Project>, String, Option<String> /*error text*/),
    /// Downloading and parsing the song
    Importing(
        Box<Project>,
        Promise<Result<vykradac::Song, (String /*URL*/, String /*error*/)>>,
    ),
    /// Allow user to make changes before saving
    Edit(Box<Project>, vykradac::Song, Option<String> /*error*/),
    /// Song saving in progress
    Saving(Box<Project>, vykradac::Song, Promise<Result<(),Option<String> /*None if user cancels saving, Some if error*/>>),
    /// Saving sucessful
    Saved(Box<Project>) //todo: add button to open the file
}

impl ImportState {
    /// draws UI based on current state and handles state change
    pub fn ui(self, ui: &mut egui::Ui) -> MainState {
        match self {
            ImportState::EnterURL(proj, url_string, err_string) => {
                let mut url_string = url_string;
                ui.heading(proj.title());
                ui.add_space(10.0);
                ui.heading("Import song");
                ui.label("Supported websites:");
                ui.hyperlink("https://pisnicky-akordy.cz/");
                ui.add_space(5.0);
                ui.horizontal(|ui| {
                    ui.label("Enter song URL:");
                    ui.add(TextEdit::singleline(&mut url_string));
                });
                let mut import = false;
                let mut back = false;
                ui.horizontal(|ui|{
                    if ui.button("cancel").clicked() {
                        back = true;
                        return;
                    }
                    if ui.button("import").clicked() {
                        import = true;
                        return;
                    }
                });
                if import {
                    return MainState::Open(OpenState::Import(ImportState::import(proj, url_string)));
                }
                if back {
                    return MainState::Open(OpenState::Overview(proj));
                }
                // show error text is the is some
                if let Some(ref err_string) = err_string {
                    ui.label(RichText::new(err_string).color(Color32::RED));
                }
                return MainState::Open(OpenState::Import(ImportState::EnterURL(
                    proj, url_string, err_string,
                )));
            }
            ImportState::Importing(proj, prom) => {
                ui.heading(proj.title());
                ui.add_space(10.0);
                ui.horizontal(|ui| {
                    ui.label("Importing...");
                    ui.spinner();
                });
                match prom.try_take() {
                    Ok(Ok(song)) => { // promise finished successfully
                        MainState::Open(OpenState::Import(ImportState::Edit(proj, song, None)))
                    }
                    Ok(Err((url_str, err_str))) => // promise finished with an error
                        MainState::Open(OpenState::Import(
                            ImportState::EnterURL(proj, url_str, Some(err_str)),
                    )),
                    Err(prom) => { //promise hasn't finished yet
                        MainState::Open(OpenState::Import(ImportState::Importing(proj, prom)))
                    }
                }
            }
            ImportState::Edit(proj, mut song, err_str) => {
                let mut back = false;
                let mut save = false;
                egui::ScrollArea::vertical().show(ui, |ui| {
                    ui.heading(proj.title());
                    ui.add_space(10.0);
                    ui.horizontal_wrapped(|ui|{
                        ui.label("Title:");
                        ui.label(RichText::new(&song.title).strong())
                    });
                    ui.horizontal_wrapped(|ui|{
                        ui.label("Artist:");
                        ui.label(RichText::new(&song.artist).strong())
                    });
                    ui.add_space(5.0);

                    // text edit
                    ui.add(
                            egui::TextEdit::multiline(&mut song.body)
                        .font(egui::TextStyle::Monospace) // for cursor height
                        .code_editor()
                        //.desired_rows(10)
                        .lock_focus(true)
                        .desired_width(f32::INFINITY)
                        //.layouter(&mut layouter),
                    );

                    ui.horizontal(|ui|{
                        if ui.button("Cancel").clicked() {
                            back = true;
                        }
                        if ui.button("Save").clicked() {
                            save = true;
                        }
                    })
                });
                if save {
                    return MainState::Open(OpenState::Import(ImportState::save(proj, song)));
                }
                if back {
                    return MainState::Open(OpenState::Overview(proj));
                }
                if let Some(ref err_str) = err_str {
                    ui.label(RichText::new(err_str).color(Color32::RED));
                }
                return MainState::Open(OpenState::Import(ImportState::Edit(proj, song,err_str)));
            },
            ImportState::Saving(proj, song, promise) => {
                ui.heading(proj.title());
                ui.add_space(10.0);
                ui.horizontal(|ui|{
                    ui.label("Saving...");
                    ui.spinner();
                });
                match promise.try_take() {
                    Ok(Ok(())) => { // promise finished successfully
                        MainState::Open(OpenState::Import(ImportState::Saved(proj)))
                    },
                    Ok(Err(err_string)) => { // promise finished with an error or user cancelled file pick dialog
                        MainState::Open(OpenState::Import(ImportState::Edit(proj, song,err_string)))
                    },
                    Err(promise) => { //promise hasn't finished yet
                        MainState::Open(OpenState::Import(ImportState::Saving(proj, song,promise)))
                    }
                }
            },
            ImportState::Saved(proj) => {
                ui.heading(proj.title());
                ui.add_space(10.0);
                ui.label(RichText::new("Song saved").color(Color32::GREEN));
                if ui.button("OK").clicked() {
                    //reload
                    return MainState::Loading(LoadingState::reload_project(proj));
                }
                return MainState::Open(OpenState::Import(ImportState::Saved(proj)));
            }
        }
    }

    fn import(proj: Box<Project>, url_str: String) -> ImportState {
        ImportState::Importing(proj, Promise::spawn_thread("import song", || {
            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();
            match Url::parse(&url_str) {
                Ok(url) => match rt.block_on(vykradac::get_song(url)) {
                    Ok(song) => Ok(song),
                    Err(e) => Err((url_str, e.to_string())),
                },
                Err(e) => Err((url_str, format!("Invalid URL address: {}", e))),
            }
        }))
    }

    /// asks user where to save the song and saves it there
    fn save(proj: Box<Project>, song: vykradac::Song) -> ImportState {
        // if directory 'songs' in the project dir exists, use it as default, otherwise the default is th rpoject dir.
        let mut default_dir = proj.project_dir.join("songs");
        if !default_dir.exists() {
            default_dir = proj.project_dir.clone();
        }
        let song2 = song.clone();
        let save_promise = Promise::spawn_thread("save song", move ||{
            if let Some(path) = rfd::FileDialog::new()
                        .set_directory(default_dir)
                        .set_file_name(&format!("{} ({}).md", &song.title, &song.artist))
                        .save_file(){
                match File::create(&path) {
                    Ok(mut file) => {
                        file.write_all(&song.body.as_bytes()).map_err(|err| format!("Saving failed: {}", err)).map_err(|e| Some(e))
                    },
                    Err(err) => {
                        Err(Some(format!("Saving failed: {}", err)))
                    }
                }
            }else{
                // user cancelled location selection
                Err(None)
            }
        });
        ImportState::Saving(proj, song2, save_promise)
    }
}
