use crate::main_state::MainState;
use crate::loading_state::LoadingState;
use crate::ProjectExt;
use crate::import_state::ImportState;

use anyhow::Result;
use bard::project::Project;

use eframe::egui::{self, Color32, RichText, TextStyle, Stroke};
use poll_promise::Promise;
use std::env;
use std::process::Command;

/// A project is open
pub enum OpenState {
    /// Project overview screen
    Overview(Box<Project>),
    /// Screen for importing songs from supported websites
    Import(ImportState),
    /// Songbook compilation in progress
    Making(Box<Project>, Promise<Result<String,String>>),
    /// Songbook compilation finished
    MakeFinished(Box<Project>, Result<String,String>)
}

impl OpenState {
    /// draws UI based on current state and handles state change
    pub fn ui(self, ui: &mut egui::Ui) -> MainState {
        match self {
            OpenState::Overview(proj) => {
                let mut close = false;

                // heading
                ui.horizontal(|ui| {
                    ui.heading(proj.title());
                    ui.with_layout(egui::Layout::right_to_left(egui::Align::TOP), |ui| {
                        if ui.add(egui::Button::new("❌ close").small()).clicked(){
                            close = true; //return MainState::NothingOpen();
                        }
                    });
                });
                if close {
                    return MainState::NothingOpen(None);
                }

                ui.add_space(10.0);

                let mut make = false;
                let mut import = false;

                // row of buttons
                ui.horizontal(|ui|{
                    if ui.button("Edit settings").clicked() {
                        //todo make the projet path pub and use it directly
                        if let Err(e) = open::that(&proj.project_dir.join("bard.toml")){
                            eprintln!("Error opening bard.toml: {}", e);
                        }
                    }
                    if ui.add(egui::Button::new("Make PDF, HTML,...").stroke(Stroke::new(0.3, Color32::YELLOW))).clicked() {
                        make = true;
                        return;
                    }
                    if ui.button("Import song").clicked() {
                        import = true;
                        return;
                    }
                });

                if make {
                    return MainState::Open(OpenState::make(proj));
                }
                if import {
                    return MainState::Open(OpenState::Import(ImportState::EnterURL(proj, String::new(), None)))
                }

                // reload button; todo: watch for file changes
                if ui.button("↻ reload").clicked() {
                    return MainState::Loading(LoadingState::reload_project(proj));
                };

                // project location
                ui.horizontal(|ui|{
                    ui.label(format!("Location: {}", &proj.project_dir.canonicalize().map(|x|x.to_string_lossy().into_owned()).unwrap_or(String::from("???"))));
                    if ui.button("📁").clicked() {
                        if let Err(e) = open::that(&proj.project_dir) {
                            eprintln!("Error opening {}: {}", &proj.project_dir.to_string_lossy(), e);
                        }
                    }
                });
                // Songs list
                let song_count = proj.book.songs.len();
                ui.collapsing(format!("Songs: {}", song_count), |ui| {
                    let row_h = ui.text_style_height(&TextStyle::Body);
                    egui::ScrollArea::vertical()
                        .auto_shrink([false; 2])
                        .show_rows(ui, row_h, song_count, |ui, row_range| {
                            for (idx, song) in proj
                                .book
                                .songs
                                .iter()
                                .enumerate()
                                .skip(row_range.start)
                                .take(row_range.end - row_range.start)
                            {
                                ui.horizontal(|ui| {
                                    ui.label(RichText::new((idx + 1).to_string()).weak());
                                    ui.label(RichText::new(song.title.to_string()));
                                    let subtitle = song
                                        .subtitles
                                        .first()
                                        .map_or("".to_string(), |x| x.to_string());
                                    ui.label(RichText::new(subtitle).text_style(TextStyle::Small));
                                    //todo: add song file location
                                });
                            }
                        });
                });
                return MainState::Open(OpenState::Overview(proj));
            }
            OpenState::Making(proj, prom) => {
                ui.horizontal(|ui| {
                    ui.heading(proj.title());
                });
                ui.add_space(10.0);
                ui.horizontal(|ui|{
                    ui.label("Making outputs (PDF,...)");
                    ui.spinner();
                });
                ui.label(RichText::new("This might take a while...").small());
                match prom.try_take() {
                    Ok(res) => {
                        return MainState::Open(OpenState::MakeFinished(proj, res));
                    },
                    Err(prom) => {
                        return MainState::Open(OpenState::Making(proj, prom));
                    }
                }
            }
            OpenState::MakeFinished(proj, res) => {
                let res2 = res.clone();

                // heading
                ui.horizontal(|ui| {
                    ui.heading(proj.title());
                });
                ui.add_space(10.0);

                // status text and buttons
                let mut back = false;
                let mut remake = false;
                ui.horizontal(|ui|{
                    if res.is_ok() {
                        ui.label("✔ Success");
                    }else{
                        ui.label(RichText::new("⚠ Error").strong().color(Color32::LIGHT_RED));
                    }
                    if ui.button("back").clicked() {
                        back = true;
                    }
                    if ui.button("↺ make again").clicked() {
                        remake = true;
                    }
                });
                if back {
                    return MainState::Open(OpenState::Overview(proj));
                }
                if remake {
                    return MainState::Open(OpenState::make(proj));
                }

                ui.separator();
                // Output files list
                if res.is_ok() {
                    for output in proj.settings.output.iter() {
                        let name = output.file.file_name().map(|x|x.to_string_lossy().into_owned()).unwrap_or_else(|| format!("{:?}",output.format));
                        ui.horizontal(|ui|{
                            ui.label(&name);
                            //button to open the output file
                            if ui.button("👁").clicked() {
                                if let Err(e) = open::that(&output.file){
                                    eprintln!("Error opening {}: {}", &output.file.to_string_lossy(), e);
                                }
                            }
                        });
                    }
                }
                ui.separator();
                // compilation log
                ui.label("Complete log:");
                ui.painter().rect_filled(
                        ui.available_rect_before_wrap(),
                    0.0,
                    Color32::BLACK,
                );
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show_viewport(ui, |ui, _viewport|{
                        ui.label(RichText::new(res.unwrap_or_else(|x|x)).family(egui::text::FontFamily::Monospace));
                    });
                return MainState::Open(OpenState::MakeFinished(proj, res2));
            }
            OpenState::Import(import_state) => import_state.ui(ui),
        }
    }

    /// Compiles the project and returns [OpenState] with the correct state.
    pub fn make(proj: Box<Project>) -> OpenState {
        let proj_path_cannon = proj.project_dir.canonicalize();
        return OpenState::Making(proj, Promise::spawn_thread("make_project", ||{ // compile in background thread
            let proj_path = proj_path_cannon.map_err(|e| format!("{}",e))?;
            let exe = env::current_exe().map_err(|e| format!("{}",e))?;
            // run itself with the `cli` argument to run the bard library cli and capture std output. See beginnig of the main function for how this hack works.
            // This hack is required, since there is no simple way to get log output from the bard library as of writing this code.
            let output = Command::new(exe)
                .current_dir(proj_path)
                .args(["cli", "make"])
                .output()
                .map_err(|e| format!("{}",e))?;
            if output.status.success() {
                return Ok(String::from_utf8_lossy(&output.stderr).to_string());
            }else{
                return Err(String::from_utf8_lossy(&output.stderr).to_string());
            }
        }));
    }
}