use crate::open_state::OpenState;
use crate::main_state::MainState;

use std::path::PathBuf;
use anyhow::Result;
use bard::project::Project;
use bard::app::App;
use bard::app::MakeOpts;
use camino::{Utf8Path, Utf8PathBuf};
use eframe::egui::{self};
use poll_promise::Promise;

/// Screen shown while loading a project
pub enum LoadingState {
    Opening(Promise<Result<Option<Project>>>),
    Creating(Promise<Result<Option<Project>>>),
    Reloading(Promise<Result<Project>>),
}

impl LoadingState {
    /// draws UI based on current state and handles state change
    pub fn ui(self, ui: &mut egui::Ui) -> MainState {
        //draw UI
        ui.horizontal(|ui| {
            ui.heading(match self {
                LoadingState::Opening(_) => "Opening project...",
                LoadingState::Creating(_) => "Creating new project...",
                LoadingState::Reloading(_) => "Opening project...",
            });
            ui.spinner();
        });

        // handle state change
        // Checks if the the promise is finished and change state if so.
        return match self {
            LoadingState::Opening(promise) => {
                match promise.try_take() {
                    Ok(Ok(Some(proj))) => MainState::Open(OpenState::Overview(Box::new(proj))), // promise finished successfully and user has chosen a project
                    Ok(Ok(None)) => MainState::NothingOpen(None), // promise finished successfully and user cancelled folder selection
                    Ok(Err(e)) => { // promise finished with an error
                        MainState::NothingOpen(Some(format!("ERROR: {}", e)))
                    }
                    Err(promise) => MainState::Loading(LoadingState::Opening(promise)), //promise hasn't finished yet
                }
            }
            LoadingState::Creating(promise) => {
                match promise.try_take() {
                    Ok(Ok(Some(proj))) => MainState::Open(OpenState::Overview(Box::new(proj))), // promise finished successfully and user has chosen a project
                    Ok(Ok(None)) => MainState::NothingOpen(None), // promise finished successfully and user cancelled folder selection
                    Ok(Err(e)) => {
                        MainState::NothingOpen(Some(format!("ERROR: {}", e))) // promise finished with an error
                    }
                    Err(promise) => MainState::Loading(LoadingState::Creating(promise)), //promise hasn't finished yet
                }
            }
            LoadingState::Reloading(promise) => {
                match promise.try_take() {
                    Ok(Ok(proj)) => MainState::Open(OpenState::Overview(Box::new(proj))), // promise finished successfully
                    Ok(Err(e)) => { // promise finished with an error
                        MainState::NothingOpen(Some(format!("ERROR: {}", e)))
                    }
                    Err(promise) => MainState::Loading(LoadingState::Reloading(promise)), //promise hasn't finished yet
                }
            }
        };
    }

    //
    // Use the following methods to create instances of LoadingState
    //

    pub fn reload_project(proj: Box<Project>) -> LoadingState {
        LoadingState::Reloading(Promise::spawn_thread("reload_project", || {
            Project::new(&LoadingState::app(), proj.project_dir)
        }))
    }

    pub fn create_project() -> LoadingState {
        // do in background
        LoadingState::Creating(Promise::spawn_thread("bard_init", || {
            // show folder picker
            if let Some(path) = rfd::FileDialog::new().pick_folder() {
                if let Some(path8) = Utf8Path::from_path(&path) {
                    match bard::bard_init_at(&LoadingState::app(), path8) {
                        Ok(()) => {
                            let project = Project::new(&LoadingState::app(), path8);
                            match project {
                                Ok(project) => Ok(Some(project)),
                                Err(e) => Err(e),
                            }
                        }
                        Err(e) => Err(e),
                    }
                } else {
                    Err(anyhow::anyhow!("Invalid path"))
                }
            } else {
                // dialog cancelled
                Ok(None)
            }
        }))
    }

    /// Show a folder picker and open a project
    pub fn open_project() -> LoadingState {
        // do in background
        LoadingState::Opening(Promise::spawn_thread("open_project", || {
            // show folder picker
            if let Some(path) = rfd::FileDialog::new().pick_folder() {
                if let Some(path8) = Utf8Path::from_path(&path) {
                    let project = Project::new(&LoadingState::app(), path8);
                    match project {
                        Ok(project) => Ok(Some(project)),
                        Err(e) => Err(e),
                    }
                } else {
                    Err(anyhow::anyhow!("Invalid path"))
                }
            } else {
                //dialog cancelled
                Ok(None)
            }
        }))
    }

    /// Tries to open project at specified path, but does not show any error if it fails
    pub fn silent_open(path: PathBuf) -> LoadingState {
        LoadingState::Opening(Promise::spawn_thread("open_project", || {
            if let Ok(path) = Utf8PathBuf::from_path_buf(path) {
                let project = Project::new(&LoadingState::app(), path);
                match project {
                    Ok(project) => Ok(Some(project)),
                    Err(_) => Ok(None),
                }
            } else {
                Ok(None)
            }
        }))
    }

    fn app() -> App {
        App::new(&MakeOpts { no_postprocess: false, keep: 0, stdio: bard::app::StdioOpts { verbose: true, quiet: false, color: Some(false) } })
    }
}