#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

mod import_state;
mod loading_state;
mod main_state;
mod open_state;

use bard::project::Project;
use eframe::egui::{self};
use std::env;
use std::ffi::OsString;

use crate::loading_state::LoadingState;
use crate::main_state::MainState;

fn main() {
    //temporary hack until it gets easy to get output from the bard library: if run with `cli` argument, run the bard cli. Example: `./bard-gui cli make`.
    let args: Vec<_> = env::args_os().collect();
    if args.len() > 1 && args.get(1) == Some(&OsString::from("cli")) {
        //bard::::use_stderr(true);
        let mut args = args.clone();
        args.remove(1);
        std::process::exit(bard::bard(&args[..]));
    }

    let options = eframe::NativeOptions {
        // initial_window_size: Some(egui::vec2(320.0, 240.0)),
        ..Default::default()
    };
    eframe::run_native(
        "Bard - songbook compiler",
        options,
        Box::new(|_cc| Ok(Box::new(MyApp::default()))),
    )
    .expect("Finished with an error :(");
}

/// State of the GUI and each "screen" is expressed using an enum. [MainState] is the top-level state of the GUI.
struct MyApp {
    main_state: Option<MainState>,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            main_state: Some(match std::env::current_dir() {
                // try if the current directory is a bard project, and open it on start if it is.
                Ok(path) => MainState::Loading(LoadingState::silent_open(path)),
                Err(_) => MainState::NothingOpen(None),
            }),
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            self.main_state = Some(
                self.main_state
                    .take()
                    .unwrap_or_else(|| MainState::NothingOpen(None))
                    .ui(ui),
            );
        });
    }
}

pub trait ProjectExt {
    fn title(&self) -> &str;
}

impl ProjectExt for Project {
    fn title(&self) -> &str {
        self.settings.book["title"].as_str().unwrap_or_else(|| {
            self.project_dir
                .iter()
                .last()
                .map(|x| x.to_str())
                .flatten()
                .unwrap_or("Untitled songbook")
        })
    }
}
