# How to build bard-gui

1) Install rust: [guide here](https://www.rust-lang.org/tools/install)
2) Install git: [https://git-scm.com/downloads](https://git-scm.com/downloads)
3) Install dependencies for tectonic - follow [tectonic docs](https://tectonic-typesetting.github.io/book/latest/howto/build-tectonic/external-dep-install.html), for Windows I recommend using cargo-vcpkg
4) (linux) Make sure you have usual c and c++ compilers installed (for example Ubuntu: `apt install build-essential`).
5) (linux) install [dependencies for file picker dialog](https://docs.rs/rfd/latest/rfd/#linux--bsd-backends)
6) run `cargo build --release` in the project directory
7) profit

## Dependencies on Windows using vcpkg

Just for convenience, here is how to install dependencies using cargo-vcpkg, according to [tectonic docs](https://tectonic-typesetting.github.io/book/latest/howto/build-tectonic/external-dep-install.html):

1) open powershell in the project directory
2) install cargo-vcpkg: `cargo install cargo-vcpkg`
3) run vcpkg build (might take a long time): `cargo vcpkg -v build`
4) set some envs:
```
$env:VCPKG_ROOT=$CARGO_TARGET_DIR
if([String]::IsNullOrWhiteSpace($env:VCPKG_ROOT)){$env:VCPKG_ROOT="$(pwd)\target"}
$env:VCPKG_ROOT="$env:VCPKG_ROOT\vcpkg"
$env:RUSTFLAGS='-Ctarget-feature=+crt-static'
$env:TECTONIC_DEP_BACKEND="vcpkg"
```
5) build: `cargo build --locked --release`
