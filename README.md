# GUI for [bard][1]

Simple GUI for the Markdown-based songbook compiler called [bard][1].

Bard is used from command line, which isn't very user-friendly for non-technical people (such as most guitarists), so this is a simple GUI to make bard easier to use.

## How to use

Just double click the binary.

TODO: add CI

## Features

- Single binary for easy distribution
- Compile bard projects without using command-line
- Embedded [tectonic][2] LaTeX engine - no need to install LaTeX externally
- Import songs from popular songbook websites using [vykradac][3]

## Development

See BUILDING.md for instruction on compiling and comments in code for documentation.

### Brief code overview

State of the GUI is described using enums. Each "screen" has its enum (`ImportState`, `OpenState`,...) which represent all the possible states of the "screen" (`OpenState::Overview`, `OpenState::Making`, `OpenState::MakeFinished`). `MainState` represents state of the entire GUI app.

The enum then has a function `ui(...) -> MainState`, which draws the ui according to the state of the enum and returns the next state. If no button was clicked, the new state is usually identical, but when, for example, navigating to a different screen, a completely different state may be returned.

More complex states may also have functions for constructing instances of such states (e.g. instances of `LoadingState::Creating` are created using `LoadingState::create_project()`, which handles creating a promise with new thread for doing slow work)

[1]: https://github.com/vojtechkral/bard
[2]: https://tectonic-typesetting.github.io/en-US/
[3]: https://gitlab.com/vitSkalicky/vykradac